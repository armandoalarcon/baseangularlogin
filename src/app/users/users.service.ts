import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from '../base.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends BaseService {

  constructor(private http: HttpClient) {super(); }

  public getAllUsers(): Observable<any[]> {
    return this.http.get<any[]>(environment.API_SECURITY + '/api/values')
    .pipe(catchError(this.handleError)     );
}

}
