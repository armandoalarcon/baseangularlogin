import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { User } from 'src/app/_entities/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];
  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.getUsers();
  }

  public getUsers(){
    this.usersService.getAllUsers().subscribe(result => {
      this.users = result;
    }, error =>{
      console.log(error);
    })
  }
}
