import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

import { LoginComponent } from './auth/login/login.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { UsersComponent } from './users/users/users.component';
import { HomeComponent } from './home/home.component';
import { TamaniosComponent } from './segmentos/tamanios/tamanios.component';
import { TamanioComponent } from './segmentos/tamanio/tamanio.component';
import { EmpresasComponent } from './jerarquias/empresas/empresas.component';
import { EmpresaComponent } from './jerarquias/empresa/empresa.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent},
  { path: 'logout', component: LogoutComponent},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard], children:[
    { path: 'users', component: UsersComponent,canActivate: [AuthGuard] },
    { path: 'tamanios', component: TamaniosComponent,canActivate: [AuthGuard] },
    { path: 'tamanio', component: TamanioComponent,canActivate: [AuthGuard] },
    { path: 'tamanio/:id', component: TamanioComponent,canActivate: [AuthGuard] },
    { path: 'empresas', component: EmpresasComponent,canActivate: [AuthGuard] },
    { path: 'empresa', component: EmpresaComponent,canActivate: [AuthGuard] },
    { path: 'empresa/:id', component: EmpresaComponent,canActivate: [AuthGuard] }
  ] },
  { path: '**', component: LoginComponent}
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
