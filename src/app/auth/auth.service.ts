import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base.service';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { User } from '../_entities/user';
import { Credentials } from '../_entities/Credentials';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  private authenticationChanged = new Subject<boolean>();
  private user = new User();
    constructor(private http: HttpClient) {
      super();
    }
  
    public authenticate (body: Credentials): Observable<any> {
      const bodyString = JSON.stringify(body);
      return this.http.post<any>(environment.API_SECURITY + '/api/account/token', bodyString)
      .     pipe(catchError(this.handleError));
  
    }

    public isAuthenticated():boolean {
        //console.log(sessionStorage.getItem('token'));
        return (!(sessionStorage.getItem('token') === undefined ||         
                sessionStorage.getItem('token') === null ||      
                sessionStorage.getItem('token') === 'null' ||
                sessionStorage.getItem('token') === 'undefined' ||
                sessionStorage.getItem('token') === ''));
    }

    public isAuthenticationChanged():any {
        return this.authenticationChanged.asObservable();
    }

    public getToken():any {
        if( sessionStorage.getItem('token') === undefined || 
        sessionStorage.getItem('token') === null ||
        sessionStorage.getItem('token') === 'null' ||
        sessionStorage.getItem('token') === 'undefined' ||
        sessionStorage.getItem('token') === '') {
        return '';
    }
        //const data = JSON.parse(sessionStorage.getItem('token'));
        const data = JSON.parse(sessionStorage.getItem('token'));
        return data;

    }

    public setData(data:any):void {
        //console.log(data);
        // const _data = JSON.stringify(data)
        // console.log(_data);
        this.setStorageToken(JSON.stringify(data.token));
        this.setStorageUser(JSON.stringify(data.user));
    }

    public failToken():void {
        this.cleanSession();
        //this.setStorageToken(undefined);
    }

    public logout():void {
        this.cleanSession();
        //this.setStorageToken(undefined);
    }

    private setStorageToken(value: any):void {
        //console.log('Set Token', value);
        sessionStorage.setItem('token', value);
        this.authenticationChanged.next(this.isAuthenticated());
    }

    private setStorageUser(value: any):void {
        //console.log('Set User', value);
        sessionStorage.setItem('user', value);
        this.authenticationChanged.next(this.isAuthenticated());
    }

    public cleanSession() {
        sessionStorage.clear();
    }

    public getUser(): User {
      //console.log(sessionStorage.getItem('user'));
      //console.log(JSON.parse(sessionStorage.getItem('user')));
      //const data = JSON.parse(sessionStorage.getItem('user'));
       this.user =  JSON.parse(sessionStorage.getItem('user'));
      //return sessionStorage.getItem('user');
      return this.user;
    }
  
}
