import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { User } from 'src/app/_entities/user';
import { Credentials } from 'src/app/_entities/Credentials';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: Credentials =new Credentials();
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {    
  }

  public login(): void {
    this.auth.authenticate(this.credentials).subscribe(data => {
      this.auth.setData(data);
      this.router.navigate(['/home']);
    });

  }

}
