import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor  {

  constructor(public auth: AuthService, private injector: Injector) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const auth = this.injector.get(AuthService);
    const token = auth.getToken();
    //console.log(token);
    if (token) {
      req = req.clone({
            setHeaders: {
              Authorization: `Bearer ${token}`
            }
      });
    }
    if (req.body instanceof FormData ) {
      console.log('Ignoring FormData to avoid setHeaders');
    } else {
      if (!req.headers.has('Content-Type')) {
        req = req.clone({
          setHeaders: {
            'Content-Type': `application/json`
          }
        });
       }
     }
     //console.log(req);
    return next.handle(req);
  }

}
