import { Component, OnInit, ViewChild } from '@angular/core';
import { Empresa } from 'src/app/_entities/Empresa';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { EmpresasService } from '../empresas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css']
})
export class EmpresasComponent implements OnInit {

  empresas: Empresa[];
  displayedColumns: string[] = ['id','codigo', 'razonSocial','alias', 'opciones'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private empresasService: EmpresasService, 
              private router: Router) { }

  ngOnInit() {
    this.getempresas();
  }

  public getempresas() {
    this.empresasService.getAll().subscribe(result => {
      this.empresas = result;
      this.dataSource = new MatTableDataSource<Empresa>(this.empresas);
      this.dataSource.paginator = this.paginator;
    });
  }

  public eliminar(empresa: Empresa){
    this.empresasService.delete(empresa.id).subscribe(
      result => {
        this.getempresas();
      }
    )
  }

  public editar(empresa: Empresa){
    this.router.navigate(['home/empresa/', empresa.id]);
  }
}
