import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { EmpresasComponent } from './empresas/empresas.component';
import { EmpresaComponent } from './empresa/empresa.component';

@NgModule({
  declarations: [EmpresasComponent, EmpresaComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class JerarquiasModule { }
