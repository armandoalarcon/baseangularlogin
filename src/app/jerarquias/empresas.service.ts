import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { Empresa } from '../_entities/Empresa';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmpresasService extends BaseService {

  constructor(private http: HttpClient) { super(); }

  public getAll(): Observable<Empresa[]> {
    return this.http.get<Empresa[]>(environment.API_SECURITY + '/api/empresas')
      .pipe(catchError(this.handleError));
  }

  public get(id: string): Observable<Empresa> {
    return this.http.get<Empresa>(environment.API_SECURITY + '/api/empresas/' + id)
      .pipe(catchError(this.handleError));
  }

  public add(empresa: Empresa): Observable<any> {
    const bodyString = JSON.stringify(empresa);
    return this.http.post<any>(environment.API_SECURITY + '/api/empresas', bodyString)
      .pipe(catchError(this.handleError));
  }

  public update(id: number, empresa: Empresa): Observable<any> {
    const bodyString = JSON.stringify(empresa);
    return this.http.put<any>(environment.API_SECURITY + '/api/empresas/' + id, bodyString)
      .pipe(catchError(this.handleError));
  }

  public delete(id: number): Observable<Empresa> {
    return this.http.delete<Empresa>(environment.API_SECURITY + '/api/empresas/' + id)
      .pipe(catchError(this.handleError));
  }
}
