import { Component, OnInit } from '@angular/core';
import { Empresa } from 'src/app/_entities/Empresa';
import { ActivatedRoute, Router } from '@angular/router';
import { EmpresasService } from '../empresas.service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  empresaId: string;
  empresa: Empresa;
  constructor(private activatedRoute: ActivatedRoute,
              private empresasService: EmpresasService, 
              private router: Router) { }

  ngOnInit() {
    this.empresa = new Empresa();
    this.activatedRoute.params.subscribe(params => {
      this.empresaId = params['id'];
      if (this.empresaId) {
        this.getEmpresas();
      }
    });
  }

  public save(form: any){
    if (this.empresa.id) {
      this.updateEmpresas();
    } else {
      this.addEmpresas();
    }
  }

  public getEmpresas() {
    this.empresasService.get(this.empresaId).subscribe(result => {
      this.empresa = result;
    })
  }

  public addEmpresas() {
    this.empresasService.add(this.empresa).subscribe(result => {
      this.router.navigate(['home/empresas']);
    })
  }

  public updateEmpresas() {
    this.empresasService.update(this.empresa.id, this.empresa).subscribe(result => {
      this.router.navigate(['home/empresas']);
    })
  }

}
