import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { UseExistingWebDriver } from 'protractor/built/driverProviders';
import { User } from '../_entities/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: User = new User();
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.user = this.auth.getUser();
  }

}
