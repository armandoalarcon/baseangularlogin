import { Component, OnInit, ViewChild } from '@angular/core';
import { TamaniosService } from '../tamanios.service';
import { Tamanio } from 'src/app/_entities/Tamanio';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
// import { MatPaginator } from '@angular/material';

@Component({
  selector: 'app-tamanios',
  templateUrl: './tamanios.component.html',
  styleUrls: ['./tamanios.component.css']
})
export class TamaniosComponent implements OnInit {

  tamanios: Tamanio[];
  displayedColumns: string[] = ['id','codigo', 'nombre', 'opciones'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private tamaniosService: TamaniosService, private router: Router) { }

  ngOnInit() {
    this.getTamanios();
  }

  public getTamanios() {
    this.tamaniosService.getAll().subscribe(result => {
      this.tamanios = result;
      this.dataSource = new MatTableDataSource<Tamanio>(this.tamanios);
      this.dataSource.paginator = this.paginator;
    });
  }

  public eliminar(tamanio: Tamanio){
    // console.log('Entra Eliminar', tamanio);
    this.tamaniosService.delete(tamanio.id).subscribe(
      result => {
        this.getTamanios();
      }
    )
  }

  public editar(tamanio: Tamanio){
    this.router.navigate(['home/tamanio/', tamanio.id]);
  }

}
