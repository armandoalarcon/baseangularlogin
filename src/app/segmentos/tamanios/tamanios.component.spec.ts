import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TamaniosComponent } from './tamanios.component';

describe('TamaniosComponent', () => {
  let component: TamaniosComponent;
  let fixture: ComponentFixture<TamaniosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TamaniosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TamaniosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
