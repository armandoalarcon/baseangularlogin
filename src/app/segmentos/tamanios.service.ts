import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { Tamanio } from '../_entities/Tamanio';

@Injectable({
  providedIn: 'root'
})
export class TamaniosService extends BaseService {

  constructor(private http: HttpClient) { super(); }

  public getAll(): Observable<Tamanio[]> {
    return this.http.get<Tamanio[]>(environment.API_SECURITY + '/api/tamanios')
      .pipe(catchError(this.handleError));
  }

  public get(id: string): Observable<Tamanio> {
    return this.http.get<Tamanio>(environment.API_SECURITY + '/api/tamanios/' + id)
      .pipe(catchError(this.handleError));
  }

  public add(tamanio: Tamanio): Observable<any> {
    const bodyString = JSON.stringify(tamanio);
    return this.http.post<any>(environment.API_SECURITY + '/api/tamanios', bodyString)
      .pipe(catchError(this.handleError));
  }

  public update(id: number, tamanio: Tamanio): Observable<any> {
    const bodyString = JSON.stringify(tamanio);
    return this.http.put<any>(environment.API_SECURITY + '/api/tamanios/' + id, bodyString)
      .pipe(catchError(this.handleError));
  }

  public delete(id: number): Observable<Tamanio> {
    return this.http.delete<Tamanio>(environment.API_SECURITY + '/api/tamanios/' + id)
      .pipe(catchError(this.handleError));
  }
}
