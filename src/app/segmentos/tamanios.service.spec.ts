import { TestBed } from '@angular/core/testing';

import { TamaniosService } from './tamanios.service';

describe('TamaniosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TamaniosService = TestBed.get(TamaniosService);
    expect(service).toBeTruthy();
  });
});
