import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TamaniosService } from '../tamanios.service';
import { Tamanio } from 'src/app/_entities/Tamanio';
import { EmpresasService } from 'src/app/jerarquias/empresas.service';
import { Empresa } from 'src/app/_entities/Empresa';


@Component({
  selector: 'app-tamanio',
  templateUrl: './tamanio.component.html',
  styleUrls: ['./tamanio.component.css']
})
export class TamanioComponent implements OnInit {

  tamanioId: string;
  tamanio: Tamanio;
  empresas: Empresa[];
  empresaId : any;
  constructor(private activatedRoute: ActivatedRoute,
              private tamaniosService: TamaniosService,
              private empresasService: EmpresasService, 
              private router: Router) { }

  ngOnInit() {
    this.getData();
    this.tamanio = new Tamanio();
    this.activatedRoute.params.subscribe(params => {
      this.tamanioId = params['id'];
      if (this.tamanioId) {
        this.getTamanio();
      }
    });
  }

  public getData(){
    this.empresasService.getAll().subscribe(result => {
      this.empresas = result;
    });
  }
  public save(form: any){
    if (this.tamanio.id) {
      this.updateTamanio();
    } else {
      this.addTamanio();
    }
  }

  public getTamanio() {
    this.tamaniosService.get(this.tamanioId).subscribe(result => {
      this.tamanio = result;
    })
  }

  public addTamanio() {
    this.tamaniosService.add(this.tamanio).subscribe(result => {
      this.router.navigate(['home/tamanios']);
    })
  }

  public updateTamanio() {
    this.tamaniosService.update(this.tamanio.id, this.tamanio).subscribe(result => {
      this.router.navigate(['home/tamanios']);
    })
  }



}
