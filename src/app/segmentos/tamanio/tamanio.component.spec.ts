import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TamanioComponent } from './tamanio.component';

describe('TamanioComponent', () => {
  let component: TamanioComponent;
  let fixture: ComponentFixture<TamanioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TamanioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TamanioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
