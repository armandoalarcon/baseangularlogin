import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TamaniosComponent } from './tamanios/tamanios.component';
import { TamanioComponent } from './tamanio/tamanio.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [TamaniosComponent, TamanioComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class SegmentosModule { }
