import { Component, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from './auth/auth.service';
import { startWith, delay } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  subscription: Subscription;
  authentication: boolean;

  constructor(private auth: AuthService) {    
  }
  
  ngAfterViewInit(): void {
    this.subscription = this.auth.isAuthenticationChanged().pipe(
      startWith(this.auth.isAuthenticated()),
      delay(0)).subscribe((value) =>
        this.authentication = value
      );
  }
}
