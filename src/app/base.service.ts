// import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ErrorMessage } from './_entities/ErrorMessage';
import { User } from './_entities/user';


// @Injectable({
//   providedIn: 'root'
// })

export abstract class BaseService {

  constructor() { }

  protected handleError (error: Response | any) {
    const errorMessage: ErrorMessage = new ErrorMessage();
    errorMessage.httpStatus = error.status;
    console.error(error);
    switch (error.status) {
      case 500:
      errorMessage.title = 'Oops! Ocurrió algo inesperado';
      errorMessage.message = 'Lo sentimos, no fue posible completar tu solicitud.';
      return throwError(errorMessage);
      case 401:
      errorMessage.title = 'Uhm! Necesitas iniciar sesión';
      errorMessage.message = error.error.message;
      return throwError(errorMessage)
      case 403:
      errorMessage.title = 'Uhm! No tienes permiso';
      errorMessage.message = error.error.message;
      return throwError(errorMessage)
      case 404:
      errorMessage.title = 'Uhm! No encontrado';
      errorMessage.message = error.error.message;
      return throwError(errorMessage);
      case 400:
      errorMessage.title = 'Oops! Algo no anda bien';
      errorMessage.message = error.error.message;
      return throwError(errorMessage);
      default:
      errorMessage.title = 'Oops! Ocurrió algo inesperado';
      errorMessage.message = 'Lo sentimos, no fue posible completar tu solicitud.';
      return throwError(errorMessage);
    }

  }

  public getUserSession(): User {
    const data = JSON.parse(sessionStorage.getItem('user'));
    return data;
  }

}

